package solo.vanremmen.oefenzittingjpav2.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class CLRService implements CommandLineRunner {

    private final Printer printer;

    @Autowired
    public CLRService(Printer printer) {
        this.printer = printer;
    }

    @Override
    public void run(String... args) {
    }

}
