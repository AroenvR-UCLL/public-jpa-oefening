package solo.vanremmen.oefenzittingjpav2.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class Printer {

    private static final Logger logger = LoggerFactory.getLogger(Printer.class);

    public Printer() {
    }

    public String print(String message) {
        logger.info(message);
        return message;
    }

}
