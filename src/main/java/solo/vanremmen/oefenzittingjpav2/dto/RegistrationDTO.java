package solo.vanremmen.oefenzittingjpav2.dto;

public class RegistrationDTO {

    private long studentId;
    private long subjectId;

    public RegistrationDTO() {
    }

    public long getStudentId() {
        return studentId;
    }

    public void setStudentId(long studentId) {
        this.studentId = studentId;
    }

    public long getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(long subjectId) {
        this.subjectId = subjectId;
    }
}
