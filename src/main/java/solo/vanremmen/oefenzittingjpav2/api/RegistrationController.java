package solo.vanremmen.oefenzittingjpav2.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import solo.vanremmen.oefenzittingjpav2.dto.RegistrationDTO;
import solo.vanremmen.oefenzittingjpav2.model.Student;
import solo.vanremmen.oefenzittingjpav2.model.Subject;
import solo.vanremmen.oefenzittingjpav2.service.StudentService;
import solo.vanremmen.oefenzittingjpav2.service.SubjectService;

import javax.transaction.Transactional;
import java.util.Set;

@RestController
@RequestMapping("api/registrations")
public class RegistrationController {

    private StudentService studentService;
    private SubjectService subjectService;

    @Autowired
    public RegistrationController(StudentService studentService, SubjectService subjectService) {
        this.studentService = studentService;
        this.subjectService = subjectService;
    }

    @Transactional
    @PostMapping
    public void addRegistration(@RequestBody RegistrationDTO registrationDTO) {
        Student theStudent = studentService.selectStudentById(registrationDTO.getStudentId()).orElseThrow();
        Subject theSubject = subjectService.selectSubjectById(registrationDTO.getSubjectId()).orElseThrow();

        theStudent.registration(theSubject);
    }
}
