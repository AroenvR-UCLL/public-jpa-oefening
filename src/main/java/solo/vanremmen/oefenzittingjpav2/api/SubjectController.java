package solo.vanremmen.oefenzittingjpav2.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import solo.vanremmen.oefenzittingjpav2.dto.RegistrationDTO;
import solo.vanremmen.oefenzittingjpav2.model.Subject;
import solo.vanremmen.oefenzittingjpav2.service.SubjectService;

import java.util.List;

@RestController
@RequestMapping("api/subjects")
public class SubjectController {

    private SubjectService subjectService;

    @Autowired
    public SubjectController(SubjectService subjectService) {
        this.subjectService = subjectService;
    }

    //Crud
    @PostMapping
    public void addAddress(@RequestBody Subject subject) {
        subjectService.insertSubject(subject);
    }

    //cRud
    @GetMapping
    public List<Subject> findAllAddresses() {
        return subjectService.selectAllSubjects();
    }

    //cruD
    @DeleteMapping("/{id}")
    public void deleteAddress(@PathVariable("id") long id) {
        Subject theSubject = subjectService.selectSubjectById(id).get();
        subjectService.removeSubject(theSubject);
    }
}
