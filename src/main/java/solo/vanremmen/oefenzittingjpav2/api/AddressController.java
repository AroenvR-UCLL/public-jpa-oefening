package solo.vanremmen.oefenzittingjpav2.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import solo.vanremmen.oefenzittingjpav2.model.Address;
import solo.vanremmen.oefenzittingjpav2.model.Student;
import solo.vanremmen.oefenzittingjpav2.service.AddressService;

import java.util.List;

@RestController
@RequestMapping("api/addresses")
public class AddressController {

    private AddressService addressService;

    @Autowired
    public AddressController(AddressService addressService) {
        this.addressService = addressService;
    }

    //Crud
    @PostMapping
    public void addAddress(@RequestBody Address address) {
        addressService.insertAddress(address);
    }

    //cRud
    @GetMapping
    public List<Address> findAllAddresses() {
        return addressService.selectAllAddresses();
    }

    //cruD
    @DeleteMapping("/{id}")
    public void deleteAddress(@PathVariable("id") long id) {
        Address theAddress = addressService.selectAddressById(id).get();
        addressService.removeAddress(theAddress);
    }
}
