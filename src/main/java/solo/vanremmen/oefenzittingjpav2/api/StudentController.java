package solo.vanremmen.oefenzittingjpav2.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import solo.vanremmen.oefenzittingjpav2.model.Student;
import solo.vanremmen.oefenzittingjpav2.model.Subject;
import solo.vanremmen.oefenzittingjpav2.service.StudentService;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("api/students")
public class StudentController {
    private final StudentService studentService;

    @Autowired
    public StudentController(StudentService studentService) {
        this.studentService = studentService;
    }

    //Crud
    @PostMapping
    public void addStudent(@RequestBody Student student) {
        studentService.insertStudent(student);
    }

    //cRud
    @GetMapping("/{id}")
    public Optional<Student> getStudentById(@PathVariable("id") Long id) {
        return studentService.selectStudentById(id);
    }

    @GetMapping() //Look at ExampleAPI Spring Data to be able to have multiple RequestParams in one method.
    public List<Student> getAllStudentsOrOneByNr(@RequestParam(value = "studentnr", required = false) String studentNr) {
        return studentService.getAllStudentsOrOneByNr(studentNr);
    }

    //crUd
    @Transactional
    @PutMapping("/{studentnr}")
    public void updateStudent(@PathVariable("studentnr") String studentNr, @RequestBody Student updatedStudent) {
        studentService.updateStudent(studentNr, updatedStudent);
    }

    //cruD
    @DeleteMapping("/{studentnr}")
    public void deleteStudent(@PathVariable("studentnr") String studentNr) {
        studentService.removeStudentByNr(studentNr);
    }
}
