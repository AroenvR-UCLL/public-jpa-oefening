package solo.vanremmen.oefenzittingjpav2.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Collections;
import java.util.Set;

@Entity
@Table(name = "subject")
public class Subject {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "teacher")
    private String teacher;

    @JsonIgnore
    @ManyToMany(mappedBy = "subjects")
    private Set<Student> studenten;

    public Subject() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTeacher() {
        return teacher;
    }

    public void setTeacher(String teacher) {
        this.teacher = teacher;
    }

    public Set<Student> getStudenten() {
        return Collections.unmodifiableSet(studenten); //encapsulation
    }

    public void setStudenten(Set<Student> studenten) {
        this.studenten = studenten;
    }

    public void registration(Student student) {
        studenten.add(student);
    }
}
