package solo.vanremmen.oefenzittingjpav2.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import solo.vanremmen.oefenzittingjpav2.model.Address;
import solo.vanremmen.oefenzittingjpav2.repo.AddressRepo;

import java.util.List;
import java.util.Optional;

@Service
public class AddressService {

    private AddressRepo addressRepo;

    @Autowired
    public AddressService(AddressRepo addressRepo) {
        this.addressRepo = addressRepo;
    }

    public Address insertAddress(Address address) {
        return addressRepo.save(address);
    }

    public List<Address> selectAllAddresses() {
        return addressRepo.findAll();
    }

    public Optional<Address> selectAddressById(Long id) {
        return addressRepo.findById(id);
    }

    public void removeAddress(Address student) {
        addressRepo.delete(student);
    }
}
