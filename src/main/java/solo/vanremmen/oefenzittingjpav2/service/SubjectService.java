package solo.vanremmen.oefenzittingjpav2.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import solo.vanremmen.oefenzittingjpav2.model.Subject;
import solo.vanremmen.oefenzittingjpav2.repo.SubjectRepo;

import java.util.List;
import java.util.Optional;

@Service
public class SubjectService {

    private SubjectRepo subjectRepo;

    @Autowired
    public SubjectService(SubjectRepo subjectRepo) {
        this.subjectRepo = subjectRepo;
    }

    public Subject insertSubject(Subject subject) {
        return subjectRepo.save(subject);
    }

    public List<Subject> selectAllSubjects() {
        return subjectRepo.findAll();
    }

    public Optional<Subject> selectSubjectById(Long id) {
        return subjectRepo.findById(id);
    }

    public void removeSubject(Subject subject) {
        subjectRepo.delete(subject);
    }

}
