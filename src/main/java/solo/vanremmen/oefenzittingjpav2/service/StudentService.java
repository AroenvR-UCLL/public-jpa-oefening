package solo.vanremmen.oefenzittingjpav2.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import solo.vanremmen.oefenzittingjpav2.model.Student;
import solo.vanremmen.oefenzittingjpav2.model.Subject;
import solo.vanremmen.oefenzittingjpav2.repo.StudentRepo;
import solo.vanremmen.oefenzittingjpav2.util.Printer;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class StudentService {

    private final StudentRepo studentRepo;
    private final Printer printer;

    @Autowired
    public StudentService(StudentRepo studentRepo, Printer printer) {
        this.studentRepo = studentRepo;
        this.printer = printer;
    }

    public Student insertStudent(Student student) {
        printer.print("StudentService - Inserting student: " + student.getName());
        return studentRepo.save(student);
    }

    public List<Student> selectAllStudents() {
        printer.print("StudentService - Selecting all students.");
        return studentRepo.findAll();
    }

    public Optional<Student> selectStudentById(Long id) {
        printer.print("StudentService - Trying to select student ID: " + id);
        return studentRepo.findById(id);
    }

    public Optional<Student> selectStudentByNr(String studentNr) {
        printer.print("StudentService - Trying to select student number: " + studentNr);
        return studentRepo.findByStudentNr(studentNr);
    }

    public void removeStudent(Student student) {
        printer.print("StudentService - Removing student: " + student.getName());
        studentRepo.delete(student);
    }

    public List<Student> getAllStudentsOrOneByNr(String studentNr) {
        if (studentNr == null) {
            return selectAllStudents();
        }
        else {
            List<Student> studentList = new ArrayList<>();
            Optional<Student> optionalStudent = selectStudentByNr(studentNr);
            if (optionalStudent.isPresent()) {
                Student student = optionalStudent.get();
                studentList.add(student);
            }
            return studentList;
        }
    }

    public void updateStudent(String studentNr, Student updatedStudent) {
        printer.print("StudentService - Updating student: " + studentNr);
        Optional<Student> optionalStudent = selectStudentByNr(studentNr);

        if (optionalStudent.isPresent()) {
            Student toUpdate = optionalStudent.get();
            toUpdate.setName(updatedStudent.getName());
            toUpdate.setStudentNr(updatedStudent.getStudentNr());
            toUpdate.setEmail(updatedStudent.getEmail());
            //theStudent.setAddress(updatedStudent.getAddress());
        }
    }

    public void removeStudentByNr(String studentNr) {
        Optional<Student> optionalStudent = selectStudentByNr(studentNr);
        //This optional is returning as an Optional.Empty. Why?
        if (optionalStudent.isPresent()) {
            Student theStudent = optionalStudent.get();
            removeStudent(theStudent);
        }
    }

    public void addSubject(String studentNr, Subject newSubject) {
        Optional<Student> theStudent = selectStudentByNr(studentNr);

        if (theStudent.isPresent()) {
            Student toUpdate = theStudent.get();
            Set<Subject> subjects = toUpdate.getSubjects();
            subjects.add(newSubject);
        }
    }
}
