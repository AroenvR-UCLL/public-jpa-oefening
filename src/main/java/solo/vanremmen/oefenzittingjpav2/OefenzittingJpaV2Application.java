package solo.vanremmen.oefenzittingjpav2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OefenzittingJpaV2Application {

    public static void main(String[] args) {
        SpringApplication.run(OefenzittingJpaV2Application.class, args);
    }

}
