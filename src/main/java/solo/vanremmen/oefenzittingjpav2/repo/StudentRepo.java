package solo.vanremmen.oefenzittingjpav2.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import solo.vanremmen.oefenzittingjpav2.model.Student;

import java.util.Optional;

@Repository
public interface StudentRepo extends JpaRepository<Student, Long> {

    Optional<Student> findByStudentNr(String studentNr);

}
