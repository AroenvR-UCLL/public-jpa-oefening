package solo.vanremmen.oefenzittingjpav2.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import solo.vanremmen.oefenzittingjpav2.model.Address;

@Repository
public interface AddressRepo extends JpaRepository<Address, Long> {

}
