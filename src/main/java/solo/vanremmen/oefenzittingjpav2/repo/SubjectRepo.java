package solo.vanremmen.oefenzittingjpav2.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import solo.vanremmen.oefenzittingjpav2.model.Subject;

@Repository
public interface SubjectRepo extends JpaRepository<Subject, Long> {

}
